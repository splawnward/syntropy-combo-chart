## @superset-ui/plugin-chart-syntropy-line-average



This plugin provides Superset Syntropy Line for Superset, which is actually a bar chart.

### Usage

Configure `key`, which can be any `string`, and register the plugin. This `key` will be used to lookup this chart throughout the app.

```js
import SyntropyLineAverageChartPlugin from '@superset-ui/plugin-chart-syntropy-line-average';

new SyntropyLineAverageChartPlugin()
  .configure({ key: 'syntropy-line-average' })
  .register();
```

Then use it via `SuperChart`. See [storybook](https://apache-superset.github.io/superset-ui/?selectedKind=plugin-chart-syntropy-line-average) for more details.

```js
<SuperChart
  chartType="syntropy-line-average"
  width={600}
  height={600}
  formData={...}
  queriesData={[{
    data: {...},
  }]}
/>
```

### File structure generated

```
├── package.json
├── README.md
├── tsconfig.json
├── src
│   ├── SyntropyLineAverage.tsx
│   ├── images
│   │   └── thumbnail.png
│   ├── index.ts
│   ├── plugin
│   │   ├── buildQuery.ts
│   │   ├── controlPanel.ts
│   │   ├── index.ts
│   │   └── transformProps.ts
│   └── types.ts
├── test
│   └── index.test.ts
└── types
    └── external.d.ts
```
