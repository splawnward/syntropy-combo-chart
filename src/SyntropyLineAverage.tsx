/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import React, { useEffect, createRef } from 'react';
import { styled } from '@superset-ui/core';
import { SyntropyLineAverageProps, SyntropyLineAverageStylesProps } from './types';
import Echart from './components/Echart';

// The following Styles component is a <div> element, which has been styled using Emotion
// For docs, visit https://emotion.sh/docs/styled

// Theming variables are provided for your use via a ThemeProvider
// imported from @superset-ui/core. For variables available, please visit
// https://github.com/apache-superset/superset-ui/blob/master/packages/superset-ui-core/src/style/index.ts

const Styles = styled.div<SyntropyLineAverageStylesProps>`
  background-color: ${({ theme }) => theme.colors.secondary.light2};
  padding: ${({ theme }) => theme.gridUnit * 4}px;
  border-radius: ${({ theme }) => theme.gridUnit * 2}px;
  height: ${({ height }) => height};
  width: ${({ width }) => width};
  overflow-y: scroll;

  h3 {
    /* You can use your props to control CSS! */
    font-size: ${({ theme, headerFontSize }) => theme.typography.sizes[headerFontSize]};
    font-weight: ${({ theme, boldText }) => theme.typography.weights[boldText ? 'bold' : 'normal']};
  }
`;

/**
 * ******************* WHAT YOU CAN BUILD HERE *******************
 *  In essence, a chart is given a few key ingredients to work with:
 *  * Data: provided via `props.data`
 *  * A DOM element
 *  * FormData (your controls!) provided as props by transformProps.ts
 */

export default function SyntropyLineAverage(props: SyntropyLineAverageProps) {
  // height and width are the height and width of the DOM element as it exists in the dashboard.
  // There is also a `data` prop, which is, of course, your DATA 🎉
  const { data, height, width } = props;

  const rootElem = createRef<HTMLDivElement>();

  // Often, you just want to get a hold of the DOM and go nuts.
  // Here, you can do that with createRef, and the useEffect hook.
  console.log('prooops', props);
  let xData = [];
  let yData = [];
  data.forEach(element => {
    if (element[props.cols[0]].length < 20) {
      xData.push(element[props.cols[0]]);
    } else {
      xData.push(element[props.cols[0]].substring(0, 18) + '...');
    }
    yData.push(element[props.cols[1]]);
  });

  let series = [{
    data: yData, 
    type: 'line',
    itemStyle: {
      color: 'rgba(173, 11, 175, 0.8)'
    },
    lineStyle: {
      color: 'rgba(173, 11, 175, 0.8)'
    },
  }];
  if (props.industryAverage != "") {
    series[0]['markLine'] = {
      symbol: 'none',
      data: [
        {
          name: 'Industry Average',
          yAxis: props.industryAverage-0,
          label: {
            formatter: 'Industry Average',
          },
          lineStyle: {
            type: 'dashed',
            color: 'rgba(3, 138, 255, 1)'
          },
        },
      ]
    };
  }
  
  series[0]['markArea'] = {data: []}

  let upperColor = "rgba(255, 0, 0, 0.75)";
  if (props.upperAreaColor) {
    let uac = props.upperAreaColor;
    console.log("upper", uac)
    upperColor = "rgba("+uac.r+","+uac.g+","+uac.b+",0.5)";
  }

  if (props.upperArea != "") {
    series[0].markArea.data.push([
      {
        name: 'Upper Limit',
        yAxis: props.upperArea-0,
        label: {show: false},
        itemStyle: {
          color: upperColor,
        }
      },
      {yAxis: Math.max(...yData)*2}
    ])
  }

  let lowerColor = "rgba(255, 0, 0, 0.75)";
  if (props.lowerAreaColor) {
    let lac = props.lowerAreaColor;
    lowerColor = "rgba("+lac.r+","+lac.g+","+lac.b+",0.5)";
  }

  if (props.lowerArea != "") {
    series[0].markArea.data.push([
      {
        name: 'Lower Limit',
        yAxis: 0,
        label: {show: false},
        itemStyle: {
          color: lowerColor,
        }
      },
      {yAxis: props.lowerArea-0}
    ])
  }

  let echartOptions = {
    title: {
      text: props.headerText,
    },
    xAxis: {
      type: 'category',
      data: xData,
      axisLabel: { interval: 0, rotate: 20 },
      name: props.xAxisLabel
    },
    yAxis: {
      type: 'value',
      name: props.yAxisLabel
    },
    series: series
  };

  console.log('Plugin props', props);
  console.log('echartOptions', echartOptions);

  return (
    <Echart ref={rootElem} width={width} height={height} echartOptions={echartOptions} />
  );
}
