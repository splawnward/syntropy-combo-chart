/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
import { 
  QueryFormData,
  supersetTheme,
  QueryFormColumn,
  DataRecord,
  DataRecordValue,
} from '@superset-ui/core';

import { EChartsCoreOption, ECharts } from 'echarts';
import { TooltipMarker } from 'echarts/types/src/util/format';
import { OptionName } from 'echarts/types/src/util/types';

export type EchartsStylesProps = {
  height: number;
  width: number;
};

export interface EchartsProps {
  height: number;
  width: number;
  echartOptions: EChartsCoreOption;
  eventHandlers?: EventHandlers;
  zrEventHandlers?: EventHandlers;
  selectedValues?: Record<number, string>;
  forceClear?: boolean;
}

export type EventHandlers = Record<string, { (props: any): void }>;

export interface EchartsHandler {
  getEchartInstance: () => ECharts | undefined;
}

export interface SyntropyLineAverageStylesProps {
  height: number;
  width: number;
  headerFontSize: keyof typeof supersetTheme.typography.sizes;
  boldText: boolean;
}

interface SyntropyLineAverageCustomizeProps {
  headerText: string;
  xAxisLabel: string;
  yAxisLabel: string;
  colorScheme?: string;
  groupBy: QueryFormColumn[];
}

export type SyntropyLineAverageQueryFormData = QueryFormData &
  SyntropyLineAverageStylesProps &
  SyntropyLineAverageCustomizeProps;

export type SyntropyLineAverageProps = SyntropyLineAverageStylesProps &
  SyntropyLineAverageCustomizeProps & {
    data: DataRecord[];
    cols: string[];
    // add typing here for the props you pass in from transformProps.ts!
  };
